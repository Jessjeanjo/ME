/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // insert code here
      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${twitter}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in a new window
         // (replace "_blank" with appropriate target attribute)
         window.open("http://www.twitter.com/Jessjeanjo", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${youtube}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in a new window
         // (replace "_blank" with appropriate target attribute)
         window.open("https://www.youtube.com/channel/UCL-wJ-8gzeS7LuGSs8eR4ag", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${linkedin}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in a new window
         // (replace "_blank" with appropriate target attribute)
         window.open("http://www.linkedin.com/Jessjeanjo", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${facebook}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in a new window
         // (replace "_blank" with appropriate target attribute)
         window.open("http://www.facebook.com/Jessjeanjo", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${github}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in a new window
         // (replace "_blank" with appropriate target attribute)
         window.open("http://www.github.com/Jessjeanjo", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${githubLink}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in a new window
         // (replace "_blank" with appropriate target attribute)
         window.open("http://www.github.com/Jessjeanjo", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${YoutubeLink}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in a new window
         // (replace "_blank" with appropriate target attribute)
         window.open("https://www.youtube.com/channel/UCL-wJ-8gzeS7LuGSs8eR4ag", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${TwitterLink}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in a new window
         // (replace "_blank" with appropriate target attribute)
         window.open("http://www.twitter.com/PROJECTJBean", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${FacebookLink}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in a new window
         // (replace "_blank" with appropriate target attribute)
         window.open("http://www.facebook.com/Jessjeanjo", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${LinkedinLink}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in a new window
         // (replace "_blank" with appropriate target attribute)
         window.open("http://www.linkedin.com/in/Jessjeanjo", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${AboutMELink}", "click", function(sym, e) {
         // insert code for mouse click here
         // Hide an element 
         sym.$("LargeHEAD").hide();
         
         // Show an element 
         sym.$("NameTextShow").show();
         
         // Hide an element 
         sym.$("NameText").hide();
         
         // Hide an element 
         sym.$("ContactHeaderText").hide();
         
         // Hide an element 
         sym.$("ProjectsHeaderText").hide();
         
         // Hide an element 
         sym.$("ProjectsBefore").hide();
         sym.$("ProjectDisplay").hide();
         sym.$("SeeMore").hide();
         // Show an element 
         sym.$("splash").show();
         sym.$("AboutME_Bio").show();
         
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${ProjectsLink}", "click", function(sym, e) {
         // insert code for mouse click here
         // Hide an element 
         sym.$("LargeHEAD").hide();
         
         // Show an element 
         sym.$("ProjectsHeaderText").show();
         
         // Hide an element 
         sym.$("NameTextShow").hide();
         sym.$("NameText").hide();
         
         // Hide an element 
         sym.$("ContactHeaderText").hide();
         // Show an element 
         sym.$("ProjectsBefore").show();
         // Hide an element 
         sym.$("splash").hide();
         sym.$("AboutME_Bio").hide();
         sym.$("ProjectDisplay").hide();
         
         
         // Show an element 
         sym.$("SeeMore").show();
         
         

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5000, function(sym, e) {
         // insert code here
      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${AboutText}", "click", function(sym, e) {
         // insert code for mouse click here
         // Hide an element 
         sym.$("ProjectsHeaderText").hide();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SeeMoreLink}", "click", function(sym, e) {
         // insert code for mouse click here
         // Hide an element 
         sym.$("ProjectDisplay").show();
         sym.$("SeeMore").hide();
         sym.$("SeeMoreLinke").hide();
         
         

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 10000, function(sym, e) {
         // insert code here
      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-13304933");