/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
            'dorsa, sans-serif': '<script src=\"http://use.edgefonts.net/dorsa:n4:all.js\"></script>',
            'advent-pro, sans-serif': '<script src=\"http://use.edgefonts.net/advent-pro:n2,n5,n7,n4,n1,n6,n3:all.js\"></script>',
            'acme, sans-serif': '<script src=\"http://use.edgefonts.net/acme:n4:all.js\"></script>',
            'megrim, fantasy': '<script src=\"http://use.edgefonts.net/megrim:n4:all.js\"></script>',
            'rouge-script, cursive': '<script src=\"http://use.edgefonts.net/rouge-script:n4:all.js\"></script>',
            'tangerine, sans-serif': '<script src=\"http://use.edgefonts.net/tangerine:n4,n7:all.js\"></script>',
            'open-sans, sans-serif': '<script src=\"http://use.edgefonts.net/open-sans:n7,i7,n8,i8,i4,n3,i3,n4,n6,i6:all.js\"></script>',
            'lato, sans-serif': '<script src=\"http://use.edgefonts.net/lato:n9,i4,n1,i7,i9,n7,i1,i3,n4,n3:all.js\"></script>'        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "width",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'YellowBack',
                            type: 'rect',
                            rect: ['-64px', '-162px', '2102px', '1339px', 'auto', 'auto'],
                            fill: ["rgba(255,255,255,1.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'github',
                            display: 'none',
                            type: 'image',
                            rect: ['1584px', '990px', '62px', '62px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"social%20network133%20%281%292.png",'0px','0px']
                        },
                        {
                            id: 'facebook',
                            display: 'none',
                            type: 'image',
                            rect: ['1770px', '998px', '46px', '46px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"social%20network99.png",'0px','0px']
                        },
                        {
                            id: 'linkedin',
                            display: 'none',
                            type: 'image',
                            rect: ['1836px', '998px', '46px', '46px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"link94.png",'0px','0px']
                        },
                        {
                            id: 'youtube',
                            display: 'none',
                            type: 'image',
                            rect: ['1651px', '998px', '46px', '46px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"logotype141.png",'0px','0px']
                        },
                        {
                            id: 'twitter',
                            display: 'none',
                            type: 'image',
                            rect: ['1697px', '990px', '62px', '62px', 'auto', 'auto'],
                            overflow: 'hidden',
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"social%20network132.png",'0px','0px']
                        },
                        {
                            id: 'ProjectsText',
                            display: 'none',
                            type: 'text',
                            rect: ['1631px', '766px', 'auto', 'auto', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<span style=\"font-family: megrim, fantasy; font-size: 35px; text-transform: uppercase; color: rgb(0, 0, 0); font-weight: 900;\">PROJECT</span><span style=\"font-family: megrim, fantasy; font-size: 35px; text-transform: uppercase; color: rgb(0, 0, 0); letter-spacing: 0px; font-weight: 900;\">S</span><span style=\"font-family: megrim, fantasy; font-size: 35px; text-transform: uppercase; color: rgb(0, 0, 0); letter-spacing: 0px;\">&nbsp;</span></p>",
                            align: "left",
                            font: ['megrim, fantasy', [60, "px"], "rgba(190,5,184,0.76)", "400", "none", "normal", "break-word", "nowrap"],
                            textStyle: ["8px", "", "", "", "none"]
                        },
                        {
                            id: 'AboutText',
                            display: 'none',
                            type: 'text',
                            rect: ['1632px', '685px', 'auto', 'auto', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<font color=\"#000000\"><span style=\"font-size: 35px; text-transform: uppercase;\">ABOUT ME</span></font></p>",
                            align: "left",
                            font: ['megrim, fantasy', [60, "px"], "rgba(190,5,184,1.00)", "900", "none", "normal", "break-word", "nowrap"],
                            textStyle: ["8px", "", "", "", "none"]
                        },
                        {
                            id: 'ProjectsBlackRect',
                            type: 'rect',
                            rect: ['754px', '-988px', '2458px', '2009px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,1)",[270,[['rgba(0,0,0,1.00)',0],['rgba(255,255,255,0.00)',100]]]],
                            stroke: [0,"rgb(0, 0, 0)","none"],
                            filter: [0, 0, 1, 1, 0, 0, 0, 0, "rgba(0,0,0,0)", 0, 0, 0],
                            transform: [[],['90'],['0','-68']]
                        },
                        {
                            id: 'splash',
                            display: 'none',
                            type: 'image',
                            rect: ['-621px', '543px', '1355px', '1528px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"splash.png",'0px','0px'],
                            transform: [[],['-496']]
                        },
                        {
                            id: 'NameText',
                            type: 'text',
                            rect: ['119px', '16px', 'auto', 'auto', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<span style=\"font-family: megrim, fantasy; font-size: 225px; font-weight: 900; background-color: rgba(254, 210, 2, 0);\">JESSICA </span><span style=\"font-family: megrim, fantasy; font-size: 225px; font-weight: 900; color: rgb(255, 255, 255); background-color: rgba(254, 210, 2, 0);\">JOSEPH</span></p>",
                            font: ['advent-pro, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "nowrap"],
                            textStyle: ["3px", "", "", "", ""]
                        },
                        {
                            id: 'NameTextShow',
                            display: 'none',
                            type: 'text',
                            rect: ['119px', '16px', 'auto', 'auto', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<span style=\"font-family: megrim, fantasy; font-size: 225px; font-weight: 900; background-color: rgba(254, 210, 2, 0);\">JESSICA JOSEPH</span></p>",
                            font: ['advent-pro, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "nowrap"],
                            textStyle: ["3px", "", "", "", ""]
                        },
                        {
                            id: 'ProjectsHeaderText',
                            display: 'none',
                            type: 'text',
                            rect: ['119px', '16px', 'auto', 'auto', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<span style=\"font-family: megrim, fantasy; font-size: 225px; font-weight: 900; background-color: rgba(254, 210, 2, 0);\">PROJECTS</span></p>",
                            font: ['advent-pro, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "nowrap"],
                            textStyle: ["3px", "", "", "", ""]
                        },
                        {
                            id: 'LargeHEAD',
                            type: 'image',
                            rect: ['-35px', '147px', '1997px', '1997px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"trans2.png",'0px','0px']
                        },
                        {
                            id: 'AboutMELink',
                            type: 'rect',
                            rect: ['1633px', '711px', '221px', '31px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,0.00)","none"]
                        },
                        {
                            id: 'ProjectsLink',
                            type: 'rect',
                            rect: ['1633px', '794px', '221px', '31px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,0.00)","none"]
                        },
                        {
                            id: 'githubLink',
                            type: 'rect',
                            rect: ['1591px', '998px', '47px', '46px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(209,126,126,0.00)"],
                            stroke: [0,"rgba(0,0,0,0.00)","none"]
                        },
                        {
                            id: 'YoutubeLink',
                            type: 'rect',
                            rect: ['1650px', '998px', '47px', '46px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(209,126,126,0.00)"],
                            stroke: [0,"rgba(0,0,0,0.00)","none"]
                        },
                        {
                            id: 'TwitterLink',
                            type: 'rect',
                            rect: ['1704px', '998px', '47px', '46px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(209,126,126,0.00)"],
                            stroke: [0,"rgba(0,0,0,0.00)","none"]
                        },
                        {
                            id: 'FacebookLink',
                            type: 'rect',
                            rect: ['1769px', '998px', '47px', '46px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(209,126,126,0.00)"],
                            stroke: [0,"rgba(0,0,0,0.00)","none"]
                        },
                        {
                            id: 'LinkedinLink',
                            type: 'rect',
                            rect: ['1835px', '998px', '47px', '46px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(209,126,126,0.00)"],
                            stroke: [0,"rgba(0,0,0,0.00)","none"]
                        },
                        {
                            id: 'SeeMore',
                            display: 'none',
                            type: 'text',
                            rect: ['766px', '701px', 'auto', 'auto', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 50px;\">WANT TO SEE MORE?</span></p>",
                            align: "left",
                            font: ['advent-pro, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'SeeMoreLink',
                            type: 'rect',
                            rect: ['768px', '710px', '419px', '45px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(255,255,255,0)"],
                            stroke: [0,"rgba(0, 0, 0, 0)","none"]
                        },
                        {
                            id: 'ProjectDisplay',
                            type: 'group',
                            rect: ['141px', '365px', '832px', '370px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'ProjectsAfter',
                                type: 'group',
                                rect: ['247', '0', '687px', '370px', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Rectangle5Copy24',
                                    type: 'rect',
                                    rect: ['0px', '453px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy25',
                                    type: 'rect',
                                    rect: ['247px', '453px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy61',
                                    type: 'rect',
                                    rect: ['493px', '448px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy59',
                                    display: 'none',
                                    type: 'rect',
                                    rect: ['493px', '226px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy51',
                                    type: 'rect',
                                    rect: ['247px', '227px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy60',
                                    type: 'rect',
                                    rect: ['493px', '0px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy64',
                                    display: 'none',
                                    type: 'rect',
                                    rect: ['739px', '448px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy63',
                                    display: 'none',
                                    type: 'rect',
                                    rect: ['739px', '228px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy62',
                                    type: 'rect',
                                    rect: ['739px', '0px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy67',
                                    type: 'rect',
                                    rect: ['985px', '448px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy66',
                                    type: 'rect',
                                    rect: ['985px', '226px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                },
                                {
                                    id: 'Rectangle5Copy65',
                                    type: 'rect',
                                    rect: ['985px', '0px', '109px', '104px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,1.00)"],
                                    stroke: [0,"rgba(0, 0, 0, 0)","none"]
                                }]
                            }]
                        },
                        {
                            id: 'ProjectsBefore',
                            display: 'none',
                            type: 'group',
                            rect: ['141px', '365px', '399px', '370px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle5',
                                type: 'rect',
                                rect: ['2px', '0px', '109px', '104px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,1.00)"],
                                stroke: [0,"rgba(0, 0, 0, 0)","none"]
                            },
                            {
                                id: 'Rectangle5Copy9',
                                type: 'rect',
                                rect: ['250px', '0px', '109px', '104px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,1.00)"],
                                stroke: [0,"rgba(0, 0, 0, 0)","none"]
                            },
                            {
                                id: 'Rectangle5Copy10',
                                type: 'rect',
                                rect: ['494px', '0px', '109px', '104px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,1.00)"],
                                stroke: [0,"rgba(0, 0, 0, 0)","none"]
                            },
                            {
                                id: 'Rectangle5Copy11',
                                type: 'rect',
                                rect: ['0px', '227px', '109px', '104px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,1.00)"],
                                stroke: [0,"rgba(0, 0, 0, 0)","none"]
                            },
                            {
                                id: 'Rectangle5Copy12',
                                type: 'rect',
                                rect: ['250px', '226px', '109px', '104px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,1.00)"],
                                stroke: [0,"rgba(0, 0, 0, 0)","none"]
                            },
                            {
                                id: 'Rectangle5Copy13',
                                type: 'rect',
                                rect: ['2px', '453px', '109px', '104px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,1.00)"],
                                stroke: [0,"rgba(0, 0, 0, 0)","none"]
                            },
                            {
                                id: 'Rectangle5Copy14',
                                type: 'rect',
                                rect: ['250px', '226px', '109px', '104px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,1.00)"],
                                stroke: [0,"rgba(0, 0, 0, 0)","none"]
                            }]
                        },
                        {
                            id: 'AboutME_Bio',
                            display: 'none',
                            type: 'text',
                            rect: ['712px', '339px', '879px', '294px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px; text-align: justify; line-height: 50px;\">​<span style=\"font-size: 30px; font-weight: 300;\">Jessica Joseph here, Jess here, coming at you from Brooklyn, NY!!</span></p><p style=\"margin: 0px; text-align: justify; line-height: 50px;\"><span style=\"font-size: 30px; font-weight: 300;\"> I am a Computer Engineering major at New York University. </span></p><p style=\"margin: 0px; text-align: justify; line-height: 50px;\"><span style=\"font-size: 30px; font-weight: 300;\">I love the way Computer Science changes the way a person thinks. </span></p><p style=\"margin: 0px; text-align: justify; line-height: 50px;\"><span style=\"font-size: 30px; font-weight: 300;\">Most of the time I’m coding, but I also love going to art galleries, working out, and vlogging. &nbsp;</span></p>",
                            align: "left",
                            font: ['lato, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", ""],
                            textStyle: ["", "", "24px", "", "none"]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 10000,
                    autoPlay: true,
                    data: [
                        [
                            "eid147",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Rectangle5Copy64}",
                            'none',
                            'none'
                        ],
                        [
                            "eid186",
                            "display",
                            3094,
                            0,
                            "linear",
                            "${Rectangle5Copy64}",
                            'none',
                            'block'
                        ],
                        [
                            "eid152",
                            "display",
                            3872,
                            0,
                            "linear",
                            "${Rectangle5Copy64}",
                            'block',
                            'block'
                        ],
                        [
                            "eid160",
                            "width",
                            10000,
                            0,
                            "linear",
                            "${splash}",
                            '1355px',
                            '1355px'
                        ],
                        [
                            "eid185",
                            "top",
                            3094,
                            0,
                            "linear",
                            "${Rectangle5Copy63}",
                            '228px',
                            '228px'
                        ],
                        [
                            "eid187",
                            "height",
                            3094,
                            0,
                            "linear",
                            "${ProjectDisplay}",
                            '370px',
                            '370px'
                        ],
                        [
                            "eid111",
                            "display",
                            0,
                            0,
                            "linear",
                            "${linkedin}",
                            'none',
                            'none'
                        ],
                        [
                            "eid122",
                            "display",
                            1849,
                            0,
                            "linear",
                            "${linkedin}",
                            'none',
                            'block'
                        ],
                        [
                            "eid131",
                            "display",
                            0,
                            0,
                            "linear",
                            "${NameTextShow}",
                            'none',
                            'none'
                        ],
                        [
                            "eid154",
                            "display",
                            10000,
                            0,
                            "linear",
                            "${NameTextShow}",
                            'none',
                            'block'
                        ],
                        [
                            "eid148",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Rectangle5Copy63}",
                            'none',
                            'none'
                        ],
                        [
                            "eid151",
                            "display",
                            2750,
                            0,
                            "linear",
                            "${Rectangle5Copy63}",
                            'none',
                            'block'
                        ],
                        [
                            "eid108",
                            "display",
                            0,
                            0,
                            "linear",
                            "${twitter}",
                            'none',
                            'none'
                        ],
                        [
                            "eid118",
                            "display",
                            1741,
                            0,
                            "linear",
                            "${twitter}",
                            'none',
                            'block'
                        ],
                        [
                            "eid181",
                            "display",
                            0,
                            0,
                            "linear",
                            "${AboutME_Bio}",
                            'none',
                            'none'
                        ],
                        [
                            "eid107",
                            "display",
                            0,
                            0,
                            "linear",
                            "${ProjectsText}",
                            'none',
                            'none'
                        ],
                        [
                            "eid115",
                            "display",
                            1733,
                            0,
                            "linear",
                            "${ProjectsText}",
                            'none',
                            'block'
                        ],
                        [
                            "eid149",
                            "display",
                            0,
                            0,
                            "linear",
                            "${Rectangle5Copy59}",
                            'none',
                            'none'
                        ],
                        [
                            "eid150",
                            "display",
                            1582,
                            0,
                            "linear",
                            "${Rectangle5Copy59}",
                            'none',
                            'block'
                        ],
                        [
                            "eid153",
                            "display",
                            0,
                            0,
                            "linear",
                            "${splash}",
                            'none',
                            'none'
                        ],
                        [
                            "eid155",
                            "display",
                            10000,
                            0,
                            "linear",
                            "${splash}",
                            'none',
                            'block'
                        ],
                        [
                            "eid180",
                            "left",
                            10000,
                            0,
                            "linear",
                            "${splash}",
                            '-621px',
                            '-621px'
                        ],
                        [
                            "eid96",
                            "skewY",
                            750,
                            0,
                            "linear",
                            "${ProjectsBlackRect}",
                            '0deg',
                            '0deg'
                        ],
                        [
                            "eid97",
                            "skewY",
                            781,
                            0,
                            "linear",
                            "${ProjectsBlackRect}",
                            '0deg',
                            '-30deg'
                        ],
                        [
                            "eid98",
                            "skewY",
                            806,
                            0,
                            "linear",
                            "${ProjectsBlackRect}",
                            '-30deg',
                            '-45deg'
                        ],
                        [
                            "eid99",
                            "skewY",
                            832,
                            0,
                            "linear",
                            "${ProjectsBlackRect}",
                            '-45deg',
                            '-55deg'
                        ],
                        [
                            "eid100",
                            "skewY",
                            856,
                            0,
                            "linear",
                            "${ProjectsBlackRect}",
                            '-55deg',
                            '-65deg'
                        ],
                        [
                            "eid101",
                            "skewY",
                            880,
                            0,
                            "linear",
                            "${ProjectsBlackRect}",
                            '-65deg',
                            '-76deg'
                        ],
                        [
                            "eid102",
                            "skewY",
                            899,
                            0,
                            "linear",
                            "${ProjectsBlackRect}",
                            '-76deg',
                            '-80deg'
                        ],
                        [
                            "eid104",
                            "skewY",
                            914,
                            0,
                            "linear",
                            "${ProjectsBlackRect}",
                            '-80deg',
                            '-89deg'
                        ],
                        [
                            "eid105",
                            "skewY",
                            1330,
                            0,
                            "linear",
                            "${ProjectsBlackRect}",
                            '-89deg',
                            '-68deg'
                        ],
                        [
                            "eid194",
                            "width",
                            3094,
                            0,
                            "linear",
                            "${ProjectsBefore}",
                            '399px',
                            '399px'
                        ],
                        [
                            "eid196",
                            "width",
                            3094,
                            0,
                            "linear",
                            "${Rectangle5Copy64}",
                            '109px',
                            '109px'
                        ],
                        [
                            "eid191",
                            "height",
                            3094,
                            0,
                            "linear",
                            "${Rectangle5Copy59}",
                            '104px',
                            '104px'
                        ],
                        [
                            "eid110",
                            "display",
                            0,
                            0,
                            "linear",
                            "${facebook}",
                            'none',
                            'none'
                        ],
                        [
                            "eid121",
                            "display",
                            1797,
                            0,
                            "linear",
                            "${facebook}",
                            'none',
                            'block'
                        ],
                        [
                            "eid192",
                            "width",
                            3094,
                            0,
                            "linear",
                            "${Rectangle5Copy59}",
                            '109px',
                            '109px'
                        ],
                        [
                            "eid126",
                            "display",
                            0,
                            0,
                            "linear",
                            "${AboutText}",
                            'none',
                            'none'
                        ],
                        [
                            "eid127",
                            "display",
                            1718,
                            0,
                            "linear",
                            "${AboutText}",
                            'none',
                            'block'
                        ],
                        [
                            "eid189",
                            "height",
                            3094,
                            0,
                            "linear",
                            "${Rectangle5Copy63}",
                            '104px',
                            '104px'
                        ],
                        [
                            "eid188",
                            "width",
                            3094,
                            0,
                            "linear",
                            "${ProjectDisplay}",
                            '832px',
                            '832px'
                        ],
                        [
                            "eid129",
                            "top",
                            2166,
                            0,
                            "linear",
                            "${ProjectsText}",
                            '766px',
                            '766px'
                        ],
                        [
                            "eid145",
                            "display",
                            0,
                            0,
                            "linear",
                            "${ProjectsBefore}",
                            'none',
                            'none'
                        ],
                        [
                            "eid183",
                            "display",
                            3094,
                            0,
                            "linear",
                            "${ProjectsBefore}",
                            'none',
                            'none'
                        ],
                        [
                            "eid195",
                            "height",
                            3094,
                            0,
                            "linear",
                            "${Rectangle5Copy64}",
                            '104px',
                            '104px'
                        ],
                        [
                            "eid69",
                            "skewX",
                            1184,
                            147,
                            "linear",
                            "${ProjectsBlackRect}",
                            '0deg',
                            '-20.11deg'
                        ],
                        [
                            "eid70",
                            "skewX",
                            1330,
                            252,
                            "linear",
                            "${ProjectsBlackRect}",
                            '-20.11deg',
                            '40deg'
                        ],
                        [
                            "eid71",
                            "skewX",
                            1582,
                            168,
                            "linear",
                            "${ProjectsBlackRect}",
                            '40deg',
                            '80deg'
                        ],
                        [
                            "eid73",
                            "skewX",
                            1750,
                            156,
                            "linear",
                            "${ProjectsBlackRect}",
                            '80deg',
                            '85deg'
                        ],
                        [
                            "eid74",
                            "skewX",
                            1906,
                            195,
                            "linear",
                            "${ProjectsBlackRect}",
                            '85deg',
                            '89deg'
                        ],
                        [
                            "eid159",
                            "height",
                            10000,
                            0,
                            "linear",
                            "${splash}",
                            '1528px',
                            '1528px'
                        ],
                        [
                            "eid143",
                            "display",
                            0,
                            0,
                            "linear",
                            "${ProjectDisplay}",
                            'none',
                            'none'
                        ],
                        [
                            "eid182",
                            "display",
                            3094,
                            0,
                            "linear",
                            "${ProjectDisplay}",
                            'none',
                            'none'
                        ],
                        [
                            "eid190",
                            "width",
                            3094,
                            0,
                            "linear",
                            "${Rectangle5Copy63}",
                            '109px',
                            '109px'
                        ],
                        [
                            "eid179",
                            "top",
                            10000,
                            0,
                            "linear",
                            "${splash}",
                            '543px',
                            '543px'
                        ],
                        [
                            "eid193",
                            "height",
                            3094,
                            0,
                            "linear",
                            "${ProjectsBefore}",
                            '370px',
                            '370px'
                        ],
                        [
                            "eid144",
                            "display",
                            0,
                            0,
                            "linear",
                            "${SeeMore}",
                            'none',
                            'none'
                        ],
                        [
                            "eid135",
                            "display",
                            0,
                            0,
                            "linear",
                            "${ProjectsHeaderText}",
                            'none',
                            'none'
                        ],
                        [
                            "eid109",
                            "display",
                            0,
                            0,
                            "linear",
                            "${youtube}",
                            'none',
                            'none'
                        ],
                        [
                            "eid119",
                            "display",
                            1741,
                            0,
                            "linear",
                            "${youtube}",
                            'none',
                            'block'
                        ],
                        [
                            "eid128",
                            "top",
                            2166,
                            0,
                            "linear",
                            "${AboutText}",
                            '685px',
                            '685px'
                        ],
                        [
                            "eid168",
                            "rotateZ",
                            10000,
                            0,
                            "linear",
                            "${splash}",
                            '-496deg',
                            '-496deg'
                        ],
                        [
                            "eid112",
                            "display",
                            0,
                            0,
                            "linear",
                            "${github}",
                            'none',
                            'none'
                        ],
                        [
                            "eid116",
                            "display",
                            1733,
                            0,
                            "linear",
                            "${github}",
                            'none',
                            'block'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-13304933");
